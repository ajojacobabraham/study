## Introduction
**Bonus shares**
- Bonus issue means an issue of additional shares to existing shareholders free of cost in proportion to their existing holding.
- It may be issued out of 
1. Free reserves
2. Securities premium
3. Capital redemption reserve account 
- Cannot be issued out of revaluation reserves 
- Also known as 'capitalization of profits'
- If the subscribed and paid up capital exceeds the authorized share capital as a result of bonus issue,a resolution shall be passed by the company at its general meeting.
- A return of bonus issue along with a copy of resolution is also required to be filed with the registrar of companies.

**Rights Issue**
- Issue of rights to a company's existing shareholders that entitles them to buy additonal shares directly from the company in proportion to their existing holdings, within a fixed time period 
- Subscription price is generally at a discount over market price 
- Rights are transferable.
- Difference between cum-right and ex-right value of the share is the value of the right.


<hr>

## Issue of bonus shares 

**Provisions of the Companies Act 2013**
Section 63 of Companies Act deals with issue of bonus shares.
- 63(1)
	- A company may issue fully paid up bonus shares to its members, in any manner whatsoever, out of-
		- 1. Free reserves
		- 2. Securities premium account 
		- 3. The capital redemption reserve account 
	- Provided no issue of bonus shares shall be made by capitalizing reserves created by revaluation of assets.

- 63(2)
	- No company shall capitalize its profits or reserves for the purpose of issuing fully paid-up bonus shares unless-
		- Authorized by its articles 
		- it has, on the recommendation of the board, been authorized in the general meeting of the company
		- It has not defaulted in payment of interest or principal in respect of fixed deposits or debt securities issued by it
		- It has not defaulted in payment of statutory dues of the employees, such as, contribution to provident fund, gratuity and bonus
		- The partly paid up shares if any outstanding on the date of allotment, are made fully paid up
		- it complies with such conditions as may be prescribed 
	- Company which has once announced the decision of its board recommending a bonus issue, shall not subsequently withdraw the same.

- 63(3)
	- Bonus shares shall not be issued in lieu of dividend.
[<img src="https://gitlab.com/ajojacobabraham/study/-/raw/af81d1af90732995ac55ff01bb13377b08208ded/Accountancy/Free%20reserves.png">]
Para 39(i) of Table F under Schedule I
Company may, upon recommendation of the board, resolve-
- a. That it is desirable to capitalize any part of free reserves and
- b. That such sum be accordingly set free for distribution in the specified manner amongst the members who would have been entitled thereto, if distributed by way of dividend and in the same proportions 
- (ii) sum aforesaid shall not be paid in cash but shall be applied, either in or towards
	- paying up amounts being unpaid on any shares 
	- paying up in full, unissued shares of the company to be allotted and distributed, credited as fully paid up, to such members in the proportions aforesaid

A securities premium account and a capital redemption reserve account may only be applied in the paying up of unissued shares to be issued to members of the company as fully paid bonus shares.

**SEBI Regulations**
SEBI (Issue of capital and disclosure requirements) Regulations, 2018

<em>Regulation 293 Condition for Bonus Issue</em>

A listed issuer shall be eligible to issue bonus shares to its members if:
- Authorized by its AOA
- It has not defaulted in payment of interest or principal in respect of fixed deposits or debt securities issued by it
- It has not defaulted in payment of statutory dues of the employees, such as, contribution to provident fund, gratuity and bonus
- The partly paid up shares if any outstanding on the date of allotment, are made fully paid up
- Any of its promoters or directors is not a fugitive economic offender.


*Regulation 294 Restrictions on a bonus issue*  

- Make a bonus issue only if it has made reservation of equity shares of the same class in favor of the holders of outstanding compulsorily convertible debt instruments if any, in proportion to the convertible part thereof.
- The equity shares so reserved, shall be issued to the holder of such convertible debt instruments or warrants at the time of conversion of such convertible debt instruments, optionally convertible instruments, as the case may be, on the same terms or same proportion at which the bonus shares were issued 
- Only made out of free reserves, SP, CRR etc
- Shall not be in lieu of dividends 
- If an issuer has issued Superior voting right equity shares to its promoters or founders, any bonus issue on the SR equity shares shall carry the same ratio of voting rights compared to ordinary shares and the SR equity shares issued in a bonus issue shall also be converted to equity shares having voting rights same as that of ordinary equity shares along with existing SR equity shares.


*Regulation 295 Completion of a bonus issue*

- An issuer 
	- announcing a bonus issue after approval by its board of directors and not requiring shareholders approval 
	- shall implement the bonus issue within 15 days from the date of approval of the issue.
- Provided that where the issuer is required to seek shareholder's approval for capitalization of profits or reserves for making the bonus issue, 
	- the bonus issue shall be implemented within two months from the date of the meeting of its BOD wherein the decision to announce the bonus issue was taken subject to approval of shareholders

- Bonus issue, once announced, shall not be withdrawn 